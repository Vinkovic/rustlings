// modules1.rs
// Make me compile! Execute `rustlings hint modules1` for hints :)



fn main(){
    sausage_factory::make_sausage::sausage();
}
mod sausage_factory{
    pub mod make_sausage{
        pub fn sausage(){
            println!("sausage!");
        }
    }
}
